const express = require('express')
const app = express()
const morgan = require('morgan')
const logger = require('./logger')
const authorize = require('./authorize')
const people = require('./routes/people')
const auth = require('./routes/auth')

// req => middleware => res

// 1. use vs route 
// 2. options - our own/ express / third party

// app.use([authorize, logger])
app.use(express.static("./methods-public"))
app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use("/api/people", people)
app.use("/login", auth)

app.listen(3000, () => {
    console.log("server is listening on port 3000...")
})
 